#!/bin/bash

# enviroment
SCRIPT_DIR=$(pwd -P)
BASE_PATH=$(dirname "$SCRIPT_DIR")

# parameters
OPTS=()
SOPTS=()
COPTS=()
PARAMS=(
"--arch=x86_64"
"--retention-tag=active+1"
"--product=cpe:/o:redhat:enterprise_linux"
"--ks-meta=redhat_ca_cert no_networks"
"--param=SET_DMESG_CHECK_KEY=yes"
"--param=NAY=yes"
"--k-opts=selinux=0 enforcing=0"
"--k-opts-post=selinux=0 enforcing=0"
)
COMMAND=""

# options
TOPO=
ALL=no
FORCE=no
BRANCH=
DEF_MATCH="driver-prototype"
LOC=(driver-Prototype--bj1 driver-Prototype--bj2 driver-Prototype)

# functions
search_available_system()
{
	local opts=("${@}")
	local raw_info=""
	raw_info=$(./parse_netqe_nic_info.sh --raw "${opts[@]}")
	awk -v t="${opts[*]}" '{
		split(t, opts, " ")
		f1 = $3
		for (i = 1; i <= length(opts); i++) {
			if (opts[i] ~ "--driver") {
				f2 = $4
			}
			if (opts[i] == "--model") {
				f3 = $6
			}
			if (opts[i] == "--speed") {
				f4 = $7
			}
		}
		f5=$12
		print f1"|"f2"|"f3"|"f4"|"f5
	}' <<< "$raw_info" | sort -u | shuf
}

search_idle_system()
{
	local system
	system=$("$BASE_PATH"/common/tools/get_system.py 2>/dev/null)
	system=$(sed -n '/avail machine begin/,/avail machine end/p' <<< "$system" | sed '1d;$d')
	echo "$system"
}

select_test_system()
{
	local a1=() a2=()

	if [[ -z $1 || -z $2 ]] ; then
		return
	fi

	readarray -t a1 <<< "$1"
	readarray -t a2 <<< "$2"
	for l1 in "${a1[@]}"; do
		t1=$(echo "$l1" | awk -F '|' '{print $1}')
		for l2 in "${a2[@]}"; do
			t2=$(echo "$l2" | awk -F '|' '{print $1}')
			if [[ "$t1" != "$t2" ]]; then
				echo "$l1" "$l2"
				return
			fi
		done
	done
	}

Usage()
{
	cat <<- END
	Tools for submitting job

	Input options:
	-D, --distro        Distro kernel
	-B, --brew          Brew kernel
	-M, --match         Match system information
	-U, --unmatch       Unmatch system information
	-d, --driver        Match nic driver information
	-m, --model         Match nic model information
	-s, --speed         Match nic speed information
	-n, --num           Match nic number
	-T, --test          Test case
	-C, --cmd           Command and reboot
	-f                  Force submit no idle system
	-a                  All match system or one system
	-b                  Current test branch
	--wb                Job white board
	--url               Fetch url

	Examples:
	# optical module eeprom extend info
	./submit_job.sh -D RHEL-8.10.0-20231121.1 --wb eeprom \\
	--test nic/functions/ethtool/sanity --unmatch wsfd-netdev -a -f \\
	-- --param=RUN_FUNC=eeprom_extended_info

	# nic consistent via leapp upgrade
	./submit_job.sh -D RHEL-8.10.0-updates-20240623.1 --wb RHEL8.10-RHEL9.5 \\
	-T nic/functions/nic_driver_info/,nic/functions/update_via_leapp,nic/functions/nic_driver_info -a -f \\
	-- --param=ORIGIN_VERSION=8.10 --param=TARGET_VERSION=9.5
	END
}

progname=${0##*/}

_at=$(getopt -o D:B:M:U:d:m:s:n:T:C:abfh \
	--long distro: \
	--long brew: \
	--long match: \
	--long unmatch: \
	--long driver: \
	--long model: \
	--long speed: \
	--long num: \
	--long wb: \
	--long url: \
	--long test: \
	--long cmd: \
	--long help \
	-n "$progname" -- "$@")
eval set -- "$_at"

while true; do
	case "$1" in
	-D|--distro)       distro="$2"; shift 2;;
	-B|--brew)         PARAMS+=("--Brew=$2"); shift 2;;
	-d|--driver)       OPTS+=("--driver $2"); shift 2;;
	-m|--model)        OPTS+=("--model $2"); shift 2;;
	-s|--speed)        OPTS+=("--speed $2"); shift 2;;
	-n|--num)          OPTS+=("--num $2"); shift 2;;
	-M|--match)        OPTS+=("--match $2"); shift 2;;
	-U|--unmatch)      OPTS+=("--unmatch $2"); shift 2;;
	-T|--test)         TEST="$2"; shift 2;;
	-C|--cmd)          COMMAND+="$2"; shift 2;;
	--wb)              PARAMS+=("--wb-comment=$2"); shift 2;;
	--url)             fetch_url="$2"; shift 2;;
	-a)                ALL="yes"; shift 1;;
	-b)                BRANCH="yes"; shift 1;;
	-f)                FORCE="yes"; shift 1;;
	--)                shift; break;;
	-h|--help)         Usage; shift 1; exit 0;;
	esac
done


if [[ -z $distro ]]; then
	echo ":::ERROR::: distro must be exist!"
	exit 1
else
	MAJOR_VER=$(echo "$distro" | awk -F"-" '{print $2}' |  awk -F"." '{print $1}')
	#MINOR_VER=$(echo "$distro" | awk -F"-" '{print $2}' |  awk -F"." '{print $1}')
	if [ "$MAJOR_VER" -le 7 ];then
		PARAMS+=("--variant=Server")
	else
		PARAMS+=("--variant=baseos")
	fi

	if [ "$MAJOR_VER" -ge 9 ]; then
		COMMAND+="update-crypto-policies --set LEGACY;"
	fi
fi

if [[ -n $BRANCH ]]; then
	p=$(git remote -v | awk '{print $2}' | head -n1)
	b=$(git symbolic-ref --short HEAD)
	PARAMS+=("--fetch-url=kernel,$p/-/archive/$b/kernel-master.tar.bz2")
elif [[ -n $fetch_url ]]; then
	PARAMS+=("--fetch-url=$fetch_url")
else
	PARAMS+=("--fetch-url=kernel,https://gitlab.cee.redhat.com/kernel-qe/kernel/-/archive/master/kernel-master.tar.bz2")
fi

if [[ -z $TEST ]]; then
	echo ":::ERROR::: test must be exist!"
	exit 1
else
	[ -f tasks.list ] && rm -rf tasks.list
	touch tasks.list
	IFS=',' read -ra test <<< "$TEST"
	for t in "${test[@]}"; do
		pushd "$BASE_PATH/$t" > /dev/null 2>&1
		lstest -t 4h >> "$SCRIPT_DIR"/tasks.list
		popd > /dev/null 2>&1
	done

	if ! grep -q "update_via_leapp" tasks.list ; then
		PARAMS+=("--kdump=netqe-bj.usersys.redhat.com:/home/kdump/vmcore")
	fi
fi

if [ -n "$COMMAND" ] ; then
	PARAMS+=("--cmd-and-reboot=$COMMAND")
fi

if grep -qE "$t.*multiHost" -R "$SCRIPT_DIR"/job_scheduler/list/ ; then
	TOPO="multiHost"
	PARAMS+=("--topo=multiHost.1.1")
fi

if [[ ${#OPTS[@]} -eq 0 && -z $DEF_MATCH ]]; then
	echo ":::ERROR::: match or driver or model or speed must be exist!"
	exit 1
else
	for ((i=0; i<${#OPTS[@]}; i++)) ; do
		if echo "${OPTS[i]}" | grep -q -- "--match" ; then
			break
		fi
	done

	if [ $i -eq ${#OPTS[@]} ]; then
		if [ "$TOPO" = "multiHost" ] ; then
			OPTS+=("--match $DEF_MATCH,$DEF_MATCH")
		else
			OPTS+=("--match $DEF_MATCH")
		fi
	fi

	for opt in "${OPTS[@]}"; do
		key=${opt%% *}
		value=${opt##* }

		if [[ $value == *","* ]]; then
			SOPTS+=($key "$([[ -z "${value%%,*}" ]] && echo "any" || echo "${value%%,*}")")
			COPTS+=($key "$([[ -z "${value##*,}" ]] && echo "any" || echo "${value##*,}")")
		else
			SOPTS+=("$key" "$value")
			COPTS+=("$key" any)
		fi
	done
fi

if [ -z "$TOPO" ]; then
	avails=$(search_available_system "${SOPTS[@]}")
	if [ -z "$avails" ] ; then
		echo ":::ERROR::: no available system!"
		exit 1
	fi

	if [ "$ALL" = "yes" ] ; then
		hosts=$avails
	else
		system=$(search_idle_system)
		pattern=$(echo "$system" | sed -n "s/('\([^']*\)'.*/\1/p" | paste -sd "|" -)
		idles=$(echo "$avails" | awk -F '|' -v pattern="$pattern" '$1 ~ pattern')

		if [ -z "$idles" ] && [ "$FORCE" = "no" ] ; then
			echo ":::WARNING::: no idle system!"
			exit 1
		elif [ -z "$idles" ] ; then
			hosts=$(echo "$avails" | head -n 1)
		else
			hosts=$(echo "$idles" | head -n 1)
		fi
	fi

	while IFS= read -r host; do
		name=$(echo "$host" | awk -F '|' '{print $1}')
		systype=$(echo "$host" | awk -F '|' '{print $5}' | awk -F '-' '{print $2}')

		driver=$(echo "$host" | awk -F '|' '{print $2}')
		[ -n "$driver" ] && PARAMS+=("--param=NIC_DRIVER=$driver ")

		model=$(echo "$host" | awk -F '|' '{print $3}')
		[ -n "$model" ] && PARAMS+=("--param=NIC_MODEL=$model")

		speed=$(echo "$host" | awk -F '|' '{print $4}' | sed 's/[^0-9]//g')
		[ -n "$speed" ] && PARAMS+=("--param=NIC_SPEED=$speed")

		runtest "$distro" --machine="$name" --systype="$systype" "${PARAMS[@]}" "$@" < tasks.list
	done <<< "$hosts"

else
	if [ $ALL = "yes" ] ; then
		echo ":::ERROR::: multihost topo need idle system!"
		exit 1
	fi

	s_avail=$(search_available_system "${SOPTS[@]}")
	c_avail=$(search_available_system "${COPTS[@]}")
	if [ -z "$s_avail" ] || [ -z "$c_avail" ] ; then
		echo ":::ERROR::: no available system!!!"
		exit 1
	fi

	system=$(search_idle_system)
	pattern=$(echo "$system" | sed -n "s/('\([^']*\)'.*/\1/p" | paste -sd "|" -)
	s_idle=$(echo "$s_avail" | awk -F '|' -v pattern="$pattern" '$1 ~ pattern')
	c_idle=$(echo "$c_avail" | awk -F '|' -v pattern="$pattern" '$1 ~ pattern')

	_prio=0
	for loc in "${LOC[@]}" ; do
		prio=3
		s=$(echo "$s_idle" | grep -E "$loc\$")
		c=$(echo "$c_idle" | grep -E "$loc\$")

		t=$(select_test_system "$s" "$c")
		server=$(echo "$t" | awk '{print $1}')
		client=$(echo "$t" | awk '{print $2}')

		if [[ -n "$server" && -n "$client" ]]; then
			break
		fi

		if [[ -z $s ]] ; then
			(( prio-= 1))
			s=$(echo "$s_avail" | grep -E "$loc\$")
		fi
		if [[ -z $c ]] ; then
			(( prio-= 1))
			c=$(echo "$c_avail" | grep -E "$loc\$")
		fi

		t=$(select_test_system "$s" "$c")
		server=$(echo "$t" | awk '{print $1}')
		client=$(echo "$t" | awk '{print $2}')
		if [[ -n $server && -n $client  ]] && [ $prio -gt $_prio ]; then
			s_cand=$server
			c_cand=$client
			_prio=$prio
		fi
	done

	if [[ -z "$server" || -z "$client" ]] ; then
		if [ "$FORCE" = "no" ]; then
			echo ":::WARNING::: no idle system!!!"
			exit 1
		fi

		if [[ -z $s_cand || -z $c_cand ]] ; then
			echo ":::ERROR::: no available system!!!"
			exit 1
		fi
		server=$s_cand
		client=$c_cand
	fi

	s_name=$(echo "$server" | awk -F '|' '{print $1}')
	s_systype=$(echo "$server" | awk -F '|' '{print $5}' | awk -F '-' '{print $2}')

	c_name=$(echo "$client" | awk -F '|' '{print $1}')
	c_systype=$(echo "$client" | awk -F '|' '{print $5}' | awk -F '-' '{print $2}')

	s_driver=$(echo "$server" | awk -F '|' '{print $2}')
	c_driver=$(echo "$client" | awk -F '|' '{print $2}')
	if [ -n "$s_driver" ] && [ -n "$c_driver" ]; then
		PARAMS+=("--param=mh-NIC_DRIVER=$s_driver,$c_driver ")
	fi

	s_model=$(echo "$server" | awk -F '|' '{print $3}')
	c_model=$(echo "$client" | awk -F '|' '{print $3}')
	if [ -n "$s_model" ] && [ -n "$c_model" ]; then
		PARAMS+=("--param=mh-NIC_MODEL=$s_model,$c_model")
	fi

	s_speed=$(echo "$info" | awk -F '|' '{print $4}' | sed 's/[^0-9]//g')
	c_speed=$(echo "$info" | awk -F '|' '{print $4}' | sed 's/[^0-9]//g')
	if [ -n "$s_speed" ] && [ -n "$c_speed" ]; then
		PARAMS+=("--param=mh-NIC_SPEED=$s_speed,$c_speed")
	fi

	runtest "$distro" --machine="$s_name,$c_name" --systype="$s_systype,$c_systype" "${PARAMS[@]}" "$@"  < tasks.list
fi

rm tasks.list
