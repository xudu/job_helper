#!/bin/sh
export LANG=en_US.utf8

export PYTHON_VERSION="3.12.2"
export PROJECT_NAME="job_helper"
export CREDITS_URL="http://netqe-bj.usersys.redhat.com/share/google/iam/report-379205.credits"

SCRIPT_DIR=$(dirname "$0")
BASE_PATH=$(dirname "$(cd "$SCRIPT_DIR" && pwd -P)")

# virtual environment
VIRTUALENV_NAME=$PROJECT_NAME
PIP_BIN="pip3 --disable-pip-version-check --no-cache-dir"

# packages
PACKAGE_PATH="$BASE_PATH/.packages"
REQUIREMENTS_LOCK="$BASE_PATH/requirements.lock"

# logs
LOG_DIR="/data/log/${PROJECT_NAME}"

# functions
create_venv() {
	if ! which pyenv > /dev/null; then
		git clone https://github.com/pyenv/pyenv.git ~/.pyenv
		git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv

		cat <<-EOF >> ~/.zshrc
		export PYENV_ROOT=\$HOME/.pyenv
		export PATH=\$PYENV_ROOT/bin:/\$PATH
		eval "\$(pyenv init -)"
		eval "\$(pyenv virtualenv-init -)"
		EOF

		source ~/.zshrc
	fi

	if ! pyenv versions | grep $PYTHON_VERSION ; then
		sudo dnf install libsqlite3x-devel
		sudo dnf groupinstall "Development Tools" "Development Libraries" -y
		pyenv install -v "$PYTHON_VERSION"
	fi

	cd "$BASE_PATH" || exit
	pyenv virtualenv $PYTHON_VERSION $VIRTUALENV_NAME
	pyenv local $VIRTUALENV_NAME
}

destroy_venv() {
	pyenv virtualenv-delete -f $VIRTUALENV_NAME
}

update_or_create_venv() {
	create_venv

	if [ -e "$PACKAGE_PATH" ]; then
		FILENAMES="$(ls "$PACKAGE_PATH")"
		INSTALLED_PACKAGES="$(${PIP_BIN} list --format freeze | sed 's/[=_-]//g')"
		NEW_PACKAGES=$(for FILENAME in $FILENAMES; do
			MATCH_KEY=$(echo "$FILENAME" | sed 's/[=_-]//g; s/\.tar.*//g; s/\.zip//g; s/\.whl//g')
			if ! echo "$INSTALLED_PACKAGES" | grep -i "$MATCH_KEY" > /dev/null; then
				echo "$PACKAGE_PATH/$FILENAME"
			fi
		done)
	elif [ -f "$REQUIREMENTS_LOCK" ]; then
		LOCK_PACKAGES=$(cat "$REQUIREMENTS_LOCK")
		INSTALLED_PACKAGES="$(${PIP_BIN} list --format freeze)"
		NEW_PACKAGES=$(for LOCK_PACKAGE in $LOCK_PACKAGES; do
			if ! echo "$INSTALLED_PACKAGES" | grep -i "$LOCK_PACKAGE"; then
				echo "$LOCK_PACKAGE"
			fi
		done)
	fi

	if [ -n "$NEW_PACKAGES" ]; then
		echo "installing $NEW_PACKAGES"
		if [ -e "$PACKAGE_PATH" ]; then
			$PIP_BIN install \
				--find-links file://"$PACKAGE_PATH" \
				--no-index \
				"$NEW_PACKAGES"
		else
			for NEW_PACKAGE in $NEW_PACKAGES; do
				$PIP_BIN install "$NEW_PACKAGE"
			done
		fi
		echo "done"
	fi
}

recreate_venv() {
	destroy_venv
	update_or_create_venv
}

lockonly_packages() {
	$PIP_BIN freeze > "$REQUIREMENTS_LOCK"
}

lock_packages() {
	if $PIP_BIN check; then
		if [ -d "$PACKAGE_PATH" ]; then
			if [ -n "${PACKAGE_PATH%/*}" ] && [ "${PACKAGE_PATH%/*}" != "/" ]; then
				cd "${PACKAGE_PATH%/*}" && rm -fr "${PACKAGE_PATH##*/}"
			fi
		fi

		$PIP_BIN freeze > "$REQUIREMENTS_LOCK"

		$PIP_BIN download \
			--no-deps \
			-r "$REQUIREMENTS_LOCK" \
			-d "$PACKAGE_PATH"
	fi
}

unlock_packages() {
	if [ -d "$PACKAGE_PATH" ]; then
		if [ -n "${PACKAGE_PATH%/*}" ] && [ "${PACKAGE_PATH%/*}" != "/" ]; then
			cd "${PACKAGE_PATH%/*}" && rm -fr "${PACKAGE_PATH##*/}"
		fi
	fi
	cd "$BASE_PATH" && rm -rf "$REQUIREMENTS_LOCK"
}

generate_configure() {
	if [ ! -f "$BASE_PATH"/etc/credits.json ]; then
		curl -kL $CREDITS_URL -o "$BASE_PATH"/etc/credits.json
	fi
}

cleanup() {
	if [ -f $LOG_DIR ] || [ -z "$(ls -A $LOG_DIR)" ]; then
		find $LOG_DIR -type f -mtime +30 -exec rm {} \;
	fi
}


case $1 in
	create|destroy|recreate|update_or_create)
		"$1"_venv
		;;
	exec)
		export PYTHONPATH="$BASE_PATH:$PYTHONPATH"
		"$@"
		;;
	install|uninstall|check|list)
		${PIP_BIN} "$@"
		;;
	lock|lockonly|unlock)
		"$1"_packages
		;;
	generate)
		"$1"_configure
		;;
	cleanup)
		$1
		;;
	*)
		echo "VirtualEnv:"
		echo "  $0 create                   Create virtualenv"
		echo "  $0 destroy                  Destroy virtualenv"
		echo "  $0 recreate                 Recreate virtualenv"
		echo "  $0 update_or_create         Update or create virtualenv"
		echo "  $0 exec                     Excecute virtualenv commands"
		echo
		echo "Packages:"
		echo "  $0 install [packages]       Install packages"
		echo "  $0 uninstall [packages]     Uninstall packages"
		echo "  $0 list                     List installed packages"
		echo "  $0 lock                     Generate requirements.lock and download packages"
		echo "  $0 lockonly                 Generate requirements.lock"
		echo
		echo "Configureion:"
		echo "  $0 generate                 Generate configurations"
		echo
		echo "Logs:"
		echo "  $0 clean                    Clean logs"
		echo
		exit 1
		;;
esac
