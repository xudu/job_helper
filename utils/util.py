# _*_ coding: utf-8 _*_
import logging
import os
import pkgutil
import signal
import socket
import time

LOG = logging.getLogger(__name__)


class TimeOut:
    def __init__(self, timeout=30):
        self.timeout = timeout

    def handler(self, signum, frame):
        raise TimeoutError("The operation timed out")

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handler)
        signal.alarm(self.timeout)

    def __exit__(self, exc_type, exc_value, exc_tb):
        signal.alarm(0)


def import_module(module_name):
    module = __import__(module_name, fromlist="dummylist")
    return module


def import_cls(cls_path, seperator="."):
    module_path, seperator, cls_name = cls_path.rpartition(seperator)
    module_obj = import_module(module_path)
    return getattr(module_obj, cls_name)


def load_all_submodules(module, raise_load_exc=False):
    sub_modules = []
    prefix = module.__name__ + "."

    for loader, sub_module_name, is_pkg in pkgutil.walk_packages(
        module.__path__, prefix
    ):
        LOG.debug("%s, %s, %s, %s" % (prefix, loader, sub_module_name, is_pkg))

        try:
            sub_module = import_module(sub_module_name)
            if not is_pkg:
                sub_modules.append(sub_module)

        except Exception as e:
            if raise_load_exc:
                raise e

            LOG.exception("Failed to import module %s" % sub_module_name)

    LOG.debug("prefix: %s, sub_modules: %s" % (prefix, sub_modules))
    return sub_modules


def loop_call_until_timeout(
    func,
    func_args=(),
    func_kwargs={},
    timeout=30,
    valid_result_func=None,
    loop_interval=3,
):
    with TimeOut(timeout):
        while True:
            result = func(*func_args, **func_kwargs)
            if valid_result_func is None:
                return result

            if valid_result_func(result):
                return result
            time.sleep(loop_interval)


def get_directory_files(path, suffix):
    file_list = []
    for _ in path:
        files = os.listdir(_)
        for file in files:
            if file.endswith(suffix):
                name, suffix = os.path.splitext(file)
                file_list.append(name)
    return file_list


def get_hostname_from_address(addr):
    try:
        hostname, _, _ = socket.gethostbyaddr(addr)
        # parse hostname
        hostname_parts = hostname.split(".")
        if len(hostname_parts) <= 2:
            return hostname, ""
        else:
            return hostname_parts[0], ".".join(hostname_parts[1:])
    except socket.herror:
        return addr, addr


def get_host_address():
    s = None
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip


def Singleton(cls):
    _instance = {}

    def _singleton(*args, **kargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kargs)
        return _instance[cls]

    return _singleton


def leftpad(row, max_len):
    pad_len = max_len - len(row)
    return ([""] * pad_len) + row if pad_len != 0 else row


def strip_leftpad(data, len):
    return data[len:]


def pad(L, rows=None, cols=None):
    try:
        max_cols = max(len(row) for row in L) if cols is None else cols
        max_rows = len(L) if rows is None else rows

        pad_rows = max_rows - len(L)

        if pad_rows:
            L = L + ([[]] * pad_rows)

        return [leftpad(row, max_cols) for row in L]
    except ValueError:
        return []
