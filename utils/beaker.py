# _*_ coding: utf-8 _*_
import logging
import os
import sys

from bkr.client import conf as bkrConfig
from bkr.common.hub import HubProxy

LOG = logging.getLogger(__name__)


class BeakerProxy(object):
    def __init__(self, logger=None, **kwargs):
        self.conf = bkrConfig
        config_file = os.environ.get("BEAKER_CLIENT_CONF", None)
        if not config_file:
            user_conf = os.path.expanduser("~/.beaker_client/config")
            old_conf = os.path.expanduser("~/.beaker")
            if os.path.exists(user_conf):
                config_file = user_conf
            elif os.path.exists(old_conf):
                config_file = old_conf
                sys.stderr.write(
                    "%s is deprecated for config, please use %s instead\n"
                    % (old_conf, user_conf)
                )
            elif os.path.exists("/etc/beaker/client.conf"):
                config_file = "/etc/beaker/client.conf"
            else:
                pass

        if config_file:
            self.conf.load_from_file(config_file)

        self.hub = HubProxy(logger=logger, conf=self.conf, **kwargs)

    def who_am_i(self):
        LOG.info(self.hub.auth.who_am_i())


if __name__ == "__main__":
    bkr_proxy = BeakerProxy()
    bkr_proxy.who_am_i()
