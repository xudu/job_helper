# _*_ coding: utf-8 _*_
import re
import requests

from prettytable import PrettyTable
from utils.gsheet import gsheet
from common.jobparser import JobParser


nicinfo_url = "https://gitlab.cee.redhat.com/kernel-qe/kernel/raw/master/networking/inventory/nic_info"


class LeappJobParser(JobParser):
    def __init__(self):
        super().__init__()

    def parse_job_data(self) -> list:
        data = super().parse_job_data()
        return self.extract_leapp_data(data)

    def extract_leapp_data(self, job_data):
        leapp_data = []

        job_id = job_data["job_id"]
        job_status = job_data["job_status"]
        for recipe in job_data["job_recipes"]:
            data = {}
            machine = recipe["recipe_hostname"]
            if job_status != "Cancelled":
                leapp_task_result = "Pass"
                for task in recipe["recipe_tasks"]:
                    task_name = task["task_name"]
                    if "update_via_leapp" in task_name:
                        leapp_task_result = task["task_result"]
                        nic_driver = (
                            task["param"]["NIC_DRIVER"]
                            if "NIC_DRIVER" in task["param"]
                            else ""
                        )
                        nic_model = (
                            task["param"]["NIC_MODEL"]
                            if "NIC_MODEL" in task["param"]
                            else ""
                        )
                        nic_speed = (
                            task["param"]["NIC_SPEED"]
                            if "NIC_SPEED" in task["param"]
                            else ""
                        )
                    elif "nic_driver_info" in task_name:
                        urls = [
                            log for log in task["task_logs"] if "_nic" in log
                        ]

                data = {
                    "Machine": machine,
                    "JobId": job_id,
                    "TestDriver": nic_driver,
                    "TestModel": nic_model,
                    "TestSpeed": nic_speed,
                }
                if len(urls) > 0:
                    nic_content = self._get(urls[-1])
                    data["Note"] = nic_content
                    data.update(self.extract_nic_info(nic_content))

                if job_status != "Completed":
                    data["Result"] = job_status
                elif leapp_task_result != "Pass":
                    data["Result"] = "UpgradeFail"
                elif len(urls) == 0:
                    data["Result"] = "LogNullFail"

                leapp_data.append(data)

        return leapp_data

    def extract_nic_info(self, nicinfo):
        res, reason = "Pass", set()

        rows = nicinfo.strip().split("\n")[2:]
        for row in rows:
            columns = [col.strip() for col in row.strip("|").split("|")]

            driver = columns[1]
            model = columns[2]
            ifacename_o = columns[4]
            mac_o = columns[5]
            ifacename_t = columns[6]
            mac_t = columns[7] if len(columns) > 7 else mac_o

            if ifacename_t == "N/A":
                res = "Fail"
                reason.add("NICNoSupp %s" % (driver + "  " + model))
            elif ifacename_o == "":
                res = "Fail"
                reason.add("NICNewSupp %s" % (driver + "  " + model))
            elif ifacename_o != ifacename_t or mac_o != mac_t:
                reason.add("NICNoConsist %s" % (driver + "  " + model))
                res = "Fail"

        return {
            "Result": res,
            "Reason": "" if not reason else "\n".join(reason),
        }

    def eliminate_duplicates(self, data):
        def key(d):
            if d.get("TestDriver") or d.get("TestModel") or d.get("TestSpeed"):
                return d["TestDriver"] + d["TestModel"] + d["TestSpeed"]
            else:
                return d["Machine"]

        def keep(d1, d2):
            prio = {
                status: i
                for i, status in enumerate(["Aborted", "Any", "Queued", "Pass"])
            }
            return prio.get(d1["Result"], 0) > prio.get(d2["Result"], 0)

        return self._eliminate(data, key_func=key, keep_func=keep)


def get_url_content(url):
    requests.packages.urllib3.disable_warnings()
    response = requests.get(url, verify=False)
    return response.text


def analyze_result(result):
    res = {}
    succs = {
        (driver[0], driver[1]): machine
        for machine, drivers in result["succ"].items()
        for driver in drivers
    }

    incompelets = []
    for machine, driver_model in result["incomplete"].items():
        for drv in driver_model:
            if drv in succs:
                incompelets.append([machine, drv[0], drv[1], succs[drv]])
            else:
                incompelets.append([machine, drv[0], drv[1], "INCOMPLETE"])
    res["Incompleted Test Task"] = incompelets

    fails = []
    for machine, driver_model in result["fail"].items():
        for drv in driver_model:
            if drv in succs:
                fails.append([machine, drv[0], drv[1], succs[drv]])
            else:
                fails.append([machine, drv[0], drv[1], ""])
    res["Failed Test Task"] = fails

    others = []
    for machine, driver_model in result["other"].items():
        for drv in driver_model:
            if drv in succs:
                others.append([machine, drv[0], drv[1], succs[drv]])
            else:
                others.append([machine, drv[0], drv[1], ""])
    res["Other failed Test Task"] = others

    for k, v in res.items():
        tb = PrettyTable()
        tb.field_names = ["machine", "driver", "model", "tested"]
        tb.title = k
        tb.add_rows(v)
        print(tb)


def leapp_analyze(project, title, properties):
    result = {"succ": {}, "fail": {}, "incomplete": {}, "other": {}}
    nicinfo = get_url_content(nicinfo_url)

    gsheet.open_gsheet(project)
    gsheet.open_worksheet(title, properties)
    records = gsheet.get_worksheet_all_records(title)

    for record in records:
        machine = record["Machine"]
        pattern = re.compile(f"^(?!#).*{machine}.*$", re.MULTILINE)
        matches = pattern.findall(nicinfo)

        driver_model = set()
        for match in matches:
            driver_model.add((match.split()[3], match.split()[5]))

        if record["Result"] in ["Aborted", "Running", "Queued"]:
            result["incomplete"].setdefault(machine, driver_model)
        elif record["Result"] != "Pass":
            fail = set()
            if record["Reason"]:
                reasons = record["Reason"].split("\n")
                for reason in reasons:
                    drv, mod = reason.split()[1], reason.split()[2]
                    if (drv, mod) in driver_model:
                        driver_model.remove((drv, mod))
                    fail.add((drv, mod))
                if driver_model:
                    result["succ"].setdefault(machine, driver_model)
                if fail:
                    result["fail"].setdefault(machine, fail)
            else:
                result["other"].setdefault(machine, driver_model)
        else:
            result["succ"].setdefault(machine, driver_model)

    analyze_result(result)
