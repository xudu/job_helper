# _*_ coding: utf-8 _*_
import re
from itertools import groupby

from common.jobparser import JobParser


class EepromJobParser(JobParser):
    def __init__(self):
        super().__init__()

    def extract_log_data(self, log_url):
        data = {}

        content = self._get(log_url)
        pattern_line = {
            "PCI": (r"bus-info: (\S+)", 1),
            "Driver": (r"driver: (\S+)", 1),
            "VendorName": (r"Vendor name\s*:\s*(\S+)", 1),
            "VendorOUI": (r"Vendor OUI\s*:\s*(\S+)", 1),
            "VendorPN": (r"Vendor PN\s*:\s*(\S+)", 1),
            "VendorRev": (r"Vendor rev\s*:\s*(\S+)", 1),
            "Compliance": (r".*(cmplnce|Compliance).*:\s*(.*)", 2),
            "TransceiverCodes": (r"Transceiver codes\s*:\s*(.*)", 1),
        }
        for k, v in pattern_line.items():
            match = re.search(v[0], content)
            data[k] = match.group(v[1]) if match else "N/A"

        pattern_lines = {
            "TransceiverType": r"^\s*Transceiver type\s*:\s*(.*)$",
        }
        for k, v in pattern_lines.items():
            pattern = re.compile(v, re.MULTILINE)
            matches = pattern.findall(content)
            matched_lines = []
            for match in matches:
                matched_lines.append(match)
            data[k] = "\n".join(matched_lines) if matched_lines else "N/A"

        return data

    def extract_eeprom_data(self, job_data):
        eeprom_data = []

        job_id = job_data["job_id"]
        job_status = job_data["job_status"]
        for recipe in job_data["job_recipes"]:
            hostname = recipe["recipe_hostname"]
            for task in recipe["recipe_tasks"]:
                task_name = task["task_name"]
                if "sanity" not in task_name:
                    continue
                if job_status == "Completed":
                    for result in task["task_results"]:
                        result_result = result["result_result"]
                        path = result["result_path"]
                        if "eeprom-extended-info" not in path:
                            continue
                        nic_name = path.split("-")[-1]
                        eeprom = {
                            "Machine": hostname,
                            "JobId": job_id,
                            "Result": result_result,
                            "NIC": nic_name,
                        }

                        for log_url in result["result_logs"]:
                            if "resultoutputfile" in log_url:
                                log_data = self.extract_log_data(log_url)
                                break

                        eeprom.update(log_data)
                        eeprom_data.append(eeprom)
                elif job_status != "Cancelled":
                    eeprom = {
                        "Machine": hostname,
                        "JobId": job_id,
                        "Result": job_status,
                    }
                    eeprom_data.append(eeprom)

        return eeprom_data

    def parse_job_data(self) -> list:
        data = super().parse_job_data()
        return self.extract_eeprom_data(data)

    def eliminate_duplicates(self, data):
        def determine(g):
            if all(d["Result"] == "Pass" for d in g):
                return "Pass"
            elif any(d["Result"] == "Aborted" for d in g):
                return "Aborted"
            else:
                return "Any"

        def keep(g1, g2):
            prio = {
                status: i for i, status in enumerate(["Aborted", "Any", "Pass"])
            }
            return prio.get(determine(g1)) > prio.get(determine(g2))

        seen = {}
        sorted_data = sorted(data, key=lambda x: x["JobId"])
        for _, g in groupby(sorted_data, key=lambda x: x["JobId"]):
            group = list(g)
            key = group[0]["Machine"]
            if key not in seen or keep(group, seen[key]):
                seen[key] = group

        return [value for valus in seen.values() for value in valus]
