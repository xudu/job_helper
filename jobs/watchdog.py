# _*_ coding: utf-8 _*_
import re

from common.jobparser import JobParser


class WatchdogJobParser(JobParser):
    def __init__(self):
        super().__init__()

    def parse_job_data(self) -> list:
        data = super().parse_job_data()
        return self.extract_watchdog_data(data)

    def extract_watchdog_data(self, job_data):
        watchdog_data = []

        job_id = job_data["job_id"]
        job_status = job_data["job_status"]
        for recipe in job_data["job_recipes"]:
            machine = recipe["recipe_hostname"]
            data = {
                "TestMachine": machine,
                "JobId": job_id,
                "Result": job_status,
            }
            task_result = "Pass"
            for task in recipe["recipe_tasks"]:
                if task_result == "Pass" and task["task_result"] != "Pass":
                    task_result = task["task_result"]

                if job_status != "Completed":
                    data["Result"] = job_status

                if task["task_role"] != "SERVERS":
                    continue

                if "watchdog" not in task["task_name"]:
                    continue

                data.update(
                    {
                        "TestMachine": machine,
                        "JobId": job_id,
                        "TestDriver": task["param"].get("NIC_DRIVER", ""),
                        "TestModel": task["param"].get("NIC_MODEL", ""),
                        "TestSpeed": task["param"].get("NIC_SPEED", ""),
                        "Result": task_result,
                    }
                )

                for result in task["task_results"]:
                    if (
                        result["result_path"]
                        != "watchdog-timeout-recover-txqtrans"
                    ):
                        continue

                    for log in result["result_logs"]:
                        if "resultoutputfile" not in log:
                            continue

                        content = self._get(log)
                        matches = re.findall(
                            r"(\[\s*FAIL.*|\[\s*WARNING.*)",
                            content,
                            re.MULTILINE,
                        )
                        if matches:
                            data["Errors"] = "\n".join(matches)
                        match = re.search(r"(\d+) ms", content)
                        if match:
                            data["Duration"] = match.group(1) + " ms"

                        if (
                            "Errors" in data
                            and "Not found timed out message" in data["Errors"]
                            and data["Result"] != "Fail"
                        ):
                            data["Result"] = "NoSupp"

                if job_status != "Cancelled":
                    if job_status != "Completed":
                        data["Result"] = job_status
                    watchdog_data.append(data)

        return watchdog_data

    def eliminate_duplicates(self, data):
        def key(d):
            if d.get("TestDriver") or d.get("TestModel") or d.get("TestSpeed"):
                return d["TestDriver"] + d["TestModel"] + d["TestSpeed"]
            else:
                return d["JobId"]

        def keep(d1, d2):
            prio = {
                status: i
                for i, status in enumerate(["Aborted", "Any", "Queued", "Pass"])
            }
            return prio.get(d1["Result"], 0) > prio.get(d2["Result"], 0)

        return self._eliminate(data, key_func=key, keep_func=keep)
