# _*_ coding: utf-8 _*_
import os
import yaml
from common.exceptions import ParamException


def load_config():
    config = {}
    etc_path = (
        os.path.abspath(os.path.dirname(os.path.dirname(__file__))) + "/etc"
    )
    for conf_name in ["config"]:
        file = os.path.join(etc_path, "%s.yaml" % conf_name)
        with open(file, "r") as f:
            config[conf_name] = yaml.load(f, Loader=yaml.SafeLoader)
    return config["config"]


def get_config(m, t):
    try:
        conf = {}
        for k, v in g_config.items():
            if m not in g_config[k]:
                conf[k] = v
            else:
                conf[k] = {}
                for k1, v1 in g_config[k][m].items():
                    if t != k1 and k1 not in conf[k]:
                        conf[k][k1] = v1
                    else:
                        conf[k].update(g_config[k][m][t])
        return conf
    except KeyError as e:
        raise ParamException()


g_config = load_config()

if __name__ == "__main__":
    print(get_config("eeprom", "rhel94"))
    print(get_config("leapp", "leapp79to810"))
