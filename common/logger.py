# _*_ coding: utf-8 _*_
import logging
import os
from logging import handlers


class Logger(object):
    def __init__(self, path):
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)

        if not os.path.exists(path):
            os.makedirs(path)
        file = path + "/helpers.log"

        ch = logging.StreamHandler()
        ch.setLevel(logging.WARNING)
        self.logger.addHandler(ch)

        rh = handlers.RotatingFileHandler(
            file, maxBytes=10 * 1024 * 1024, backupCount=3
        )
        rh.setFormatter(
            logging.Formatter("%(asctime)s - %(process)d - %(levelname)s - %(message)s")
        )
        rh.setLevel(logging.DEBUG)
        self.logger.addHandler(rh)

    def getlog(self):
        return self.logger


def setup_logging(path):
    return Logger(path).getlog()


if __name__ == "__main__":
    setup_logging("./logs")
    LOG = logging.getLogger(__name__)
    LOG.debug("Debug test.")
    LOG.warning("Warning test.")
