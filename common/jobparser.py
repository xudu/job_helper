# _*_ coding: utf-8 _*_
import logging
import time

import requests
from lxml import etree as ET

LOG = logging.getLogger(__name__)


def wrap_retry(max_times=3, seconds=1):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            logging.getLogger("urllib3").setLevel(logging.WARNING)
            for cnt in range(max_times):
                try:
                    return func(self, *args, **kwargs)
                except Exception as e:
                    if cnt < (max_times - 1):
                        LOG.warning(f"Operation retry {cnt} {e}")
                        time.sleep(seconds)
                    else:
                        LOG.error(e)
                        return None

        return wrapper

    return decorator


class JobParser(object):
    def __init__(self):
        pass

    @wrap_retry(10)
    def _get(self, url):
        requests.packages.urllib3.disable_warnings()
        response = requests.get(url, verify=False)
        return response.text

    def _parse_result_data(self, root, parse_logs=True):
        data = {}
        path = root.get("path")
        if path == "Setup" or path == "Cleanup":
            return None
        else:
            data.setdefault("result_path", path)
        data.setdefault("result_result", root.get("result"))
        if parse_logs:
            data.setdefault("result_logs", [])
            for log in root.getiterator("log"):
                link = log.get("href")
                data["result_logs"].append(link)
        return data

    def _parse_task_data(self, root, pares_results=True):
        data = {}
        task_name = root.get("name")
        if task_name.find("/kernel/networking") == -1:
            return None
        else:
            data.setdefault("task_name", task_name)
            data.setdefault("task_result", root.get("result"))
            data.setdefault("task_status", root.get("status"))
            data.setdefault("task_role", root.get("role"))

        data.setdefault("param", {})
        for param in root.getiterator("param"):
            param_name = param.get("name")
            param_value = param.get("value")
            data["param"].setdefault(param_name, param_value)

        task_logs = root.find(".//logs[1]")
        data.setdefault("task_logs", [])
        for log in task_logs.getiterator("log"):
            task_log_link = log.get("href")
            task_log_name = log.get("name")
            if task_log_name in ["harness.log", "journal.xml", "taskout.log"]:
                continue
            data["task_logs"].append(task_log_link)

        if pares_results:
            data.setdefault("task_results", [])
            for result in root.getiterator("result"):
                result_data = self._parse_result_data(result)
                if result_data:
                    data["task_results"].append(result_data)
        return data

    def _parse_recipe_data(self, root, parse_tasks=True):
        data = {}
        data.setdefault("recipe_id", root.get("id"))

        hostname_elements = root.findall(".//hostname")
        for element in hostname_elements:
            data.setdefault("recipe_hostname", element.get("value"))

        if parse_tasks:
            data.setdefault("recipe_tasks", [])
            for task in root.getiterator("task"):
                task_data = self._parse_task_data(task)
                if task_data:
                    data["recipe_tasks"].append(task_data)
        return data

    def _pasre_job_data(self, root, pasre_recipes=True):
        root = self.tree.getroot()
        data = {}
        for job in root.getiterator("job"):
            data.setdefault("job_status", job.get("status"))
        job_id = job.get("id")
        data.setdefault("job_id", "J:" + job_id)
        data.setdefault(
            "job_owner", job.get("owner").replace("@redhat.com", "")
        )
        data.setdefault(
            "job_link",
            "https://beaker.engineering.redhat.com/jobs/" + str(job_id),
        )
        data.update(data)

        if pasre_recipes:
            data.setdefault("job_recipes", [])
            for recipe in root.getiterator("recipe"):
                recipe_data = self._parse_recipe_data(recipe)
                data["job_recipes"].append(recipe_data)

        return data

    def load_job_xml_file(self, xml_file):
        self.xml_file = xml_file
        self.tree = ET.parse(xml_file)

    def parse_job_data(self):
        root = self.tree.getroot()
        data = self._pasre_job_data(root)
        LOG.info(data)
        return data

    def _eliminate(self, data, key_func, keep_func):
        seen = {}

        for item in data:
            key = key_func(item)

            if key not in seen or keep_func(item, seen[key]):
                seen[key] = item

        return [value for _, value in sorted(seen.items())]


if __name__ == "__main__":
    import os

    job = "J:8605292"
    print("Dump eeprom %s xml" % job)
    os.system(f"bkr job-results --prettyxml {job} > /dev/shm/{job}.xml")
    parser = JobParser()
    parser.load_job_xml_file(f"/dev/shm/{job}.xml")
    print(parser.parse_job_data())

    job = "J:9273918"
    print("Dump leapp %s xml" % job)
    os.system(f"bkr job-results --prettyxml {job} > /dev/shm/{job}.xml")
    parser = JobParser()
    parser.load_job_xml_file(f"/dev/shm/{job}.xml")
    print(parser.parse_job_data())
