# _*_ coding: utf-8 _*_
import logging

from common.constants import *

LOG = logging.getLogger(__name__)


class JobHelpersException(Exception):
    code = INTERNAL_ERROR
    message = "Unknown exception occurred."

    def __init__(self, *args, **kwargs):
        try:
            self.code = args[0] if args else self.code
            self.message = args[1] if len(args) > 1 else self.message
            self.data = kwargs
            super(JobHelpersException, self).__init__(self.message)
        except Exception as e:
            super(JobHelpersException, self).__init__()
            LOG.error(e)

    def __str__(self):
        return self.message


class BeakerException(JobHelpersException):
    code = BEAKER_ERROR
    message = "Could not login beaker"


class ParamException(JobHelpersException):
    code = CONFIG_ERROR
    message = "Confige file parameters missing"
