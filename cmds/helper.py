# _*_ coding: utf-8 _*_
import argparse
import logging
import sys
import traceback

from common.config import get_config
from common.jobmanager import JobManager
from common.logger import setup_logging
from utils.gsheet import Formatter, gsheet
from jobs.leapp import leapp_analyze

LOG = logging.getLogger(__name__)


def collect_jobs(cfg):
    try:
        jobcfg, shcfg = cfg["job"], cfg["sheet"]

        owner = jobcfg.get("owner")
        wb = jobcfg.get("whiteboard")
        allow_dup = jobcfg.get("allow_dup", True)
        jobmgr = JobManager(jobcfg["parser"])
        results = jobmgr.batch_jobs_result(
            owner=owner, whiteboard=wb, allow_dup=allow_dup
        )

        project = shcfg["project"]
        owners = shcfg["owners"]
        gsheet.open_gsheet(project, owners=owners)

        title = shcfg["title"]
        properties = {
            "orig": tuple(shcfg["orig"]),
            "range": tuple(shcfg["range"]),
            "fields": shcfg["fields"],
        }
        gsheet.open_worksheet(title=title, properties=properties)

        gsheet.refill_worksheet_all_records(title, results)
        formts = shcfg["format"]
        gsheet.batch_format_worksheet_row_records(
            title,
            formts,
        )
    except Exception as e:
        traceback.print_exc()


def monitor_jobs():
    pass


def analyze_jobs(module, config):
    shcfg = config["sheet"]
    project = shcfg["project"]
    title = shcfg["title"]
    properties = {
        "orig": tuple(shcfg["orig"]),
        "range": tuple(shcfg["range"]),
        "fields": shcfg["fields"],
    }
    if module == "leapp":
        leapp_analyze(project, title, properties)


def usage():
    print(
        """
Usage: bin/environ.sh exec python3 cmds/helper.py -m [module] -t [test] --collect

-- module      The testing module
-- test        The testing name
-- collect     Collecting job execution results, analyzing data, and recording outcomes.
-- monitor     Monitoring job runs, real-time data analysis, and result updates.
-- analyze     Analyze job result.

The script assists in managing job tasks, tracks job executions, analyzes job data, and records job results..

You must setup two files with tokens and parameters:

./etc/credits.joson

The script uses a gspread authentication token file for Google Sheets authentication, utilizing the default "network-driver" project.
It creates different sheets within this shared project. See the address below.

https://console.cloud.google.com/iam-admin/serviceaccounts/details/112076677080414733823/keys?project=report-379205

./etc/common.toml

Configuration file that defines attributes, permissions, and other parameters for the job and google sheet in use.

Example: bin/environ.sh exec python3 cmds/helper.py -m eeprom -t rhel94 --collect

	"""
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--module", help="testing module")
    parser.add_argument("-t", "--test", help="testing name")
    parser.add_argument(
        "--collect", action="store_true", help="collect job result"
    )
    parser.add_argument(
        "--monitor", action="store_true", help="monitor job result"
    )
    parser.add_argument(
        "--analyze", action="store_true", help="analyze job result"
    )

    args = parser.parse_args()
    if args.module is None or args.test is None:
        usage()
        sys.exit(1)

    conf = get_config(args.module, args.test)
    setup_logging(conf["log"]["path"])

    if "whiteboard" not in conf["job"]:
        conf["job"]["whiteboard"] = args.test
    if "title" not in conf["sheet"]:
        conf["sheet"]["title"] = args.test

    if args.collect:
        collect_jobs(conf)
    elif args.monitor:
        monitor_jobs(conf)
    elif args.analyze:
        analyze_jobs(args.module, conf)

    sys.exit(0)
